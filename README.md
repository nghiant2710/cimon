# Welcome to _cimon_
_cimon_ is an node.js app which acts an event-source for my thermometer view at [123k.de](http://123k.de). It periodically reads the temperature from a simple USB thermometer and remembers that value. Other jobs come along an push this value via MQTT to the frontend application. Please also have a look at the frontend code. You can find it also at Bitbucket - [https://bitbucket.org/efwe/pluskwa](https://bitbucket.org/efwe/pluskwa).

Have any suggestions? 
Tell me about it on Twitter - [@\_efwe\_](https://twitter.com/_efwe_). And always remember _Don't forget to have fun!_ 

Note: I'm also an alpha-tester for the cool guys at [resin.io](http://resin.io) - so if you really want to use this app you may want to change the ```package.json``` again.