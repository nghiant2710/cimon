# usb client (https://github.com/node-hid/node-hid)
HID = require('node-hid')
# mqtt client (https://github.com/adamvr/MQTT.js)
mqtt = require('mqtt')
settings = {
  keepalive: 1000,
  protocolId: 'MQIsdp',
  protocolVersion: 3,
  clientId: 'cimon'
}
mqtt = mqtt.createClient('1883', 'owca.123k.org', settings)


# usb-bridge (https://github.com/node-hid/node-hid)
findDevice = () ->
  devices = HID.devices()
  for device in devices
    if device['vendorId'] is 3141 and device['product'] is 'TEMPerV1.4' and device['interface'] is 1
      return new HID.HID(device['path'])

# stolen from node-temper (https://github.com/asmuelle/node-temper1)
renderData = (hiByte, loByte) ->
  sign = hiByte & (1 << 7)
  temp = ((hiByte & 0x7F) << 8) | loByte
  if (sign)
    temp = -temp
  return temp * 125.0 / 32000.0



# trigger onData (registered with the device) by writing a read command to the device
readTemperature = (device) ->
  device.write [0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00]
  #console.log 'wrote read command to USB device'


# callback for the usb-data returned after read command
# just refreshes @currentTemperature
onData = (data) =>
  temperature = renderData data[2], data[3]
  msg = {
      "location":"office",
      "time": new Date(),
      "temp": temperature
    }

  # this is essentially a shared resource - maybe i need https://www.npmjs.org/package/rwlock
  @currentTemperature = JSON.stringify(msg)


# find device
device = findDevice(onData)
# register onData handler
device.on "data", onData

# send temperature-message to 123k.de
publishTemperature = (mqtt) =>
  if @currentTemperature
    # body...
    mqtt.publish 'temp-stream', @currentTemperature
  else
    console.log "no temperature available to publish"

# schedule events
setInterval readTemperature, 10 * 1000, device
setInterval publishTemperature, 15 * 1000, mqtt

console.log " cimon booted at:'#{new Date()}'."
